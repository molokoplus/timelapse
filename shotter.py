import time
import sys
from datetime import datetime

import gphoto2cffi as gp


def shot(extension='jpeg'):
    """
    Take a photo and save in a file
    """
    cam = gp.Camera()
    file_name = '{}.{}'.format(datetime.now().isoformat(), extension)

    with open(file_name, 'wb') as f:
        f.write(cam.capture())


if __name__ == '__main__':
    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        sys.exit()