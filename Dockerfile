FROM python:3.6.6-alpine3.8

RUN apk update && apk add build-base libffi-dev gphoto2 libgphoto2 libgphoto2-dev py-cffi

RUN mkdir /shots
WORKDIR /shots

ADD requirements.txt .

RUN pip install -r requirements.txt

ADD shotter.py .